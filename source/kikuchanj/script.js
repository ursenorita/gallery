var viewModel = function() {
  var self = this;

  self.images = ko.observable([]);
  self.showImages = ko.observable(false);

  self.getImages = function() {
    console.log("loading images...");
    fetch('./data.json').then((response)=>{
        if(!response.ok) throw new Error("http error.")
        return response.json()
    }).then(data => {
        self.images(data);
        console.log("images loaded");
    })
  };

  self.images.subscribe(function() {
    setTimeout(function() {
      self.showImages(true);
      console.log("masonry...");
      var $container = $('#container');
      console.log("$container", $container);

      $container.imagesLoaded(function() {
        console.log("imagesLoaded...");
        $container.masonry({
          itemSelector: '.item',
          columnWidth: function(containerWidth) {
            return containerWidth / 12;
          }
        });
        $('.item img').addClass('not-loaded');
        $('.item img.not-loaded').lazyload({
          effect: 'fadeIn',
          load: function() {
            // Disable trigger on this image
            console.log("loading image...");
            $(this).removeClass("not-loaded");
            $container.masonry('reload');
          }
        });
        $('.item img.not-loaded').trigger('scroll');
      });
      console.log("masonry complete");
    }, 1000);
  });

  self.getImages();

  return self;
}

ko.applyBindings(new viewModel());